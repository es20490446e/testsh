```
USAGE
       testsh () {
            source "$(lib)/testsh"
            runTests
       }

       if [[ "${*}" == "-test" ]]; then
            testsh
       else
            mainFunction
       fi

WHAT IS RUN
       - Tests are searched inside the script dir.

       - If functions starting with "ATEST_" exist, they are run.

       - If no "ATEST_" exists, all "TEST_" are run.

       - Name any test that you don't want to be run as "DTEST_".

DIRECTIVES
       - Succeeds or is true: _ye command

       - Fails or is false: _no command

       - Output is correct: _eq "${expectsRegex}" "${gets[@]}"

RECOMENDATIONS
       -  A  test  is always considered failed if it has output. Further tests
       are cancelled.

       - Preceed any other command with "_ye". So its output is silenced,  ex‐
       cept on error where a trace is printed.

       - Check that the test also fails when it shall.

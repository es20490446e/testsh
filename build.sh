#! /bin/bash
set -e

here="$(realpath "$(dirname "${0}")")"
assets="${here}/assets"
root="${here}/_ROOT"
lib="${root}$(lib)"


mainFunction () {
	rm --recursive --force "${root}"
	rsyncFile "${assets}/testsh" "${lib}/testsh"
	rsyncFile "${assets}/testsh.1" "${root}/usr/share/man/man1/testsh.1"
}


rsyncFile () {
	local In="${1}"
	local Out="${2}"

	cd "${here}"
	local Dir="$(dirname "${Out}")"

	mkdir --parents "${Dir}"
	rsync --archive --no-owner --no-group "${In}" "${Out}" >/dev/null
}


mainFunction
